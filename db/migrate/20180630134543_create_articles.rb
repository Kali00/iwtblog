class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :body

      t.timestamps#creates two columns "created_at" & "updated_at"
    end
  end
end
